## etherpad-lite
Create a subdomain with "proxy" and "http://your-server-ip:9001"

Uncheck "Don't add to Let's Encrypt certificate"

Add the corresponding Proxy directives under the options tab.
